<?php

namespace App\BASIS\SEIP106215\Email\Utility;

class Utility{
    
    
    static public function d($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    
    static public function redirect($url="/AtomicProject_Oliur_SEIP106215/View/BITM/SEIP106215/Email/index.php"){
        header("Location:".$url);
    }
    
    static public function massage($massage = null){
        
        if(is_null($massage)){
            
        $_massage = $_SESSION['massage'];
        
        $_SESSION['massage'] = null;
        
        return $_massage;
        
        }  else {
            
           $_SESSION['massage'] = $massage; 
        }
        
//        var_dump($_SESSION);die(); 
    }
    
    
}

