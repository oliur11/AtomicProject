<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'AtomicProject_Oliur_SEIP106215'.DIRECTORY_SEPARATOR.'View'.DIRECTORY_SEPARATOR.'BITM'.DIRECTORY_SEPARATOR.'startup.php');

use App\BASIS\SEIP106215\Radio\Gender;
use App\BASIS\SEIP106215\Radio\Utility\Utility;

$gender = new Gender();

$genders = $gender->index();
?>





<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Gender</title>
        <link href="../../../../Resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <style>
            #utility{
                float: right;
            }
            #mass{
                
                background-color: greenyellow;
                
            }

        </style>
    </head>
    <body>
        <?php ?>

        <div class="container">
            <div class="row">
                <div class="well">
                    <div style="height: 900px" class="col-ms-3">
                        <table class="table table-hover">

                            <div><h3>Gender List</h3></div>
                            <br/>
                            <div id="mass"><?php echo Utility::massage(); ?></div>
                            <br/>
                            <div><span><a href="create.php">Add New</a></span> | 
                                <select>
                                    <option>5</option>
                                    <option>10</option>
                                    <option>15</option>
                                    <option>20</option>
                                </select>

                                <span id="utility">Download PDF | Docx</span>
                            </div>

                            <tr>
                                <th>ID.</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>


                            <?php
                            $idno = 1;
                            foreach ($genders as $gender) {
                                ?>

                                <tr>
                                    <td><?php echo $idno; ?></td> 
                                    <td><?php echo $gender->name; ?></td>
                                    <td><?php echo $gender->title; ?></td>  
                                    <td>
                                        <a class="btn btn-primary btn-md" href="view.php">View</a> | 
                                        <a class="btn btn-info btn-md" href="edit.php">Edit</a> | 
                                        <a class="btn btn-danger btn-md" href="delete.php">Delete</a> | 
                                        <a class="btn btn-warning" href="#">Trash/Recovery</a>
                                    </td>                             
                                </tr>

                                <?php
                                $idno++;
                            }
                            ?>

                        </table>
                        <nav style="float: right; padding-right: 470px">
                            <ul class="pagination">
                                <li>
                                    <a href="#" aria-label="Previous">
                                        <span aria-hidden="true"><<</span>
                                    </a>
                                </li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li>
                                    <a href="#" aria-label="Next">
                                        <span aria-hidden="true">>></span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>